﻿using Windows.ApplicationModel.Background;
using Windows.Networking.PushNotifications;
using Windows.UI.Notifications;

namespace FZS.Runtime
{
    /// <summary>
    /// Raw通知后台接收处理
    /// </summary>
    public sealed class ToastBackgroundTask : IBackgroundTask
    {
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            var deferral = taskInstance.GetDeferral();

            #region 原始通知内容处理

            var raw = taskInstance.TriggerDetails as RawNotification;
            if (raw != null)
            {
                new MessageProcess.RawMessageProcess().OpenMessage(raw.Content);

                deferral.Complete();
                return;
            }

            #endregion

            deferral.Complete();
        }
    }
}
