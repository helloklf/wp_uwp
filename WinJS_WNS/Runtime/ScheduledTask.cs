﻿using FZS.Runtime.MessageProcess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Networking.PushNotifications;
using Windows.System;
using Windows.UI.Notifications;

namespace FZS.Runtime
{
    /// <summary>
    /// 定时任务
    ///! 更新过期的消息通道（目前不可用）
    /// </summary>
    public sealed class ScheduledTask : IBackgroundTask
    {
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            /*
            #region 检验通道是否改变（该方法并不稳定）
            try
            {
                var notification = (await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync());
                
                //过期时间
                var expirationDate = notification.ExpirationTime.Date;

                var channel = notification.Uri;

                DateTimeOffset date = notification.ExpirationTime;
                var localChannel = new Core.Services.MessagesService().GetLocalCurrentChannelURI();
                
                if (channel.ToString().ToLower() != localChannel?.ToLower())
                {
                    new Core.Services.MessagesService().SetLocalCurrentChannelURI(channel.ToString());
                    var msgContent = "<toast><visual><binding template='ToastGeneric'><text>消息通道变了啊！！！</text></binding></visual></toast>";
                    var xmlDoc = new Windows.Data.Xml.Dom.XmlDocument();
                    xmlDoc.LoadXml(msgContent);
                    ToastNotification tn = new ToastNotification(xmlDoc);
                    ToastNotificationManager.CreateToastNotifier().Show(tn);
                }
            }
            catch (Exception ex)
            {

            }
#endregion
    */
        }
    }
}
