﻿using System;
using Windows.ApplicationModel.Background;
using Windows.Storage;
using Windows.System;
using Windows.UI.Notifications;

namespace FZS.Runtime
{
    /// <summary>
    /// Toast通知事件后台处理（由于暂无Toast交互操作业务需求，该任务目前被废弃）
    /// 处理toast的点击动作的
    /// </summary>
    public sealed class ToastActionBackgroundTask : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            var deferral = taskInstance.GetDeferral();
            var details = taskInstance.TriggerDetails as ToastNotificationActionTriggerDetail;
            if (details != null)
            {
                string arg = details.Argument?.Trim().ToLower();
                if (arg != "" && arg != "cancel")
                {

                }
            }
            deferral.Complete();
        }
    }
}
