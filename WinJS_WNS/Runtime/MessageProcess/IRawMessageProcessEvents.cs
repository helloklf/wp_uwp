﻿using System;

namespace FZS.Runtime.MessageProcess
{
    /// <summary>
    /// 消息事件回调
    /// </summary>
    public interface IRawMessageProcessEvents
    {
        /// <summary>
        /// 当UWP消息接收
        /// </summary>
        /// <param name="msgContent"></param>
        void OnUwpMessageReceived(PushedMessageInfo msgContent);

        /// <summary>
        /// 当Tile消息接收
        /// </summary>
        /// <param name="msgContent"></param>
        void OnTileMessageReceived(PushedMessageInfo msgContent);

        /// <summary>
        /// 当Toast消息接收
        /// </summary>
        /// <param name="msgContent"></param>
        void OnToastMessageReceived(PushedMessageInfo msgContent);

        /// <summary>
        /// 当Badge消息接收
        /// </summary>
        /// <param name="msgContent"></param>
        void OnBadgeMessageReceived(PushedMessageInfo msgContent);

        /// <summary>
        /// 当未知类型的消息接收（预留）
        /// </summary>
        /// <param name="msgContent"></param>
        void OnUnknownMessageReceived(PushedMessageInfo msgContent);

    }
}
