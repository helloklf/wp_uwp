﻿using System;

namespace FZS.Runtime.MessageProcess.Status
{
    public sealed class MessageStatusCodes
    {
        /// <summary>
        ///  根据消息状态码获取消息状态说明
        /// </summary>
        /// <param name="satus">状态说明</param>
        /// <returns></returns>
        public static String MessageStatusEnumToDesc(MessageStatusEnum satus)
        {
            String statusDesc;
            switch (satus)
            {
                case MessageStatusEnum.@new:
                    {
                        statusDesc = "未读消息";
                        break;
                    }
                case MessageStatusEnum.received:
                    {
                        statusDesc = "消息已接收";
                        break;
                    }
                case MessageStatusEnum.opened:
                    {
                        statusDesc = "消息已阅读";
                        break;
                    }
                case MessageStatusEnum.mistake:
                    {
                        statusDesc = "消息发错用户";
                        break;
                    }
                case MessageStatusEnum.formaterror:
                    {
                        statusDesc = "消息格式错误";
                        break;
                    }
                default:
                    {
                        statusDesc = "未知状态";
                        break;
                    }
            }
            return statusDesc;
        }

        /// <summary>
        /// 状态码转枚举类型
        /// </summary>
        /// <param name="statusCode">状态码，如：received，忽略大小写。</param>
        /// <param name="defaultValue">状态码不在预期范围时的默认值</param>
        /// <returns></returns>
        public static MessageStatusEnum MessageStatusCodeToEnum(String statusCode,MessageStatusEnum defaultValue)
        {
            if (string.IsNullOrWhiteSpace(statusCode))
                return defaultValue;

            var code = statusCode.Trim().ToLower();

            try
            {
                return (MessageStatusEnum)(Enum.Parse(typeof(MessageStatusEnum), statusCode, true));
            }
            catch
            {
                return defaultValue;
            }

        }

        /// <summary>
        /// 新消息
        /// </summary>
        const string New = "new";

        /// <summary>
        /// 消息已接收
        /// </summary>
        const string Received = "received";

        /// <summary>
        /// 已打开
        /// </summary>
        const string Opened = "opened";

        /// <summary>
        /// 发错用户
        /// </summary>
        const string Mistake = "mistake";
        
        /// <summary>
        /// 格式错误
        /// </summary>
        const string FormatError = "formaterror";

        /// <summary>
        /// 过期
        /// </summary>
        const string TimeOut = "timeout";

        /// <summary>
        /// 预留
        /// </summary>
        const string Unknown = "unknown"; 
    }

}
