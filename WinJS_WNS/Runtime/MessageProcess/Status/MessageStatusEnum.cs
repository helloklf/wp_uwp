namespace FZS.Runtime.MessageProcess.Status
{
    /// <summary>
    /// 消息状态枚举
    /// </summary>
    public enum MessageStatusEnum
    {
        /*
            received 已接收，
            opened 已打开，
            mistake 发错用户，
            formaterror 格式错误,
            new  新消息,
            timeout 过期
        */
        /// <summary>
        /// 未知
        /// </summary>
        unknown = -2000,

        /// <summary>
        /// 新消息
        /// </summary>
        @new = 100,

        /// <summary>
        /// 消息已接收
        /// </summary>
        received = 200,

        /// <summary>
        /// 已打开
        /// </summary>
        opened = 300,

        /// <summary>
        /// 过期
        /// </summary>
        timeout = 400,

        /// <summary>
        /// 发错用户
        /// </summary>
        mistake = -400,

        /// <summary>
        /// 格式错误
        /// </summary>
        formaterror = -500
    }

}