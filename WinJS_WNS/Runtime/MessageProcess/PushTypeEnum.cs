namespace FZS.Runtime.MessageProcess
{

    /// <summary>
    /// 推送类型
    /// </summary>
    public enum PushTypeEnum
    {
        /// <summary>
        /// 未知类型
        /// </summary>
        Unknown,

        /// <summary>
        /// UWP通知
        /// </summary>
        UWP,

        /// <summary>
        /// Toast通知
        /// </summary>
        Toast,

        /// <summary>
        /// 磁贴通知
        /// </summary>
        Tile,

        /// <summary>
        /// 徽章通知
        /// </summary>
        Badge
    }

}