﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace FZS.Runtime.MessageProcess
{

    /// <summary>
    /// 推送的消息格式数据模型
    /// </summary>
    public sealed class PushedMessageInfo
    {
        public PushedMessageInfo(String json)
        {
            JsonDeSerializer(json);
        }
        
        /// <summary>
        /// Json反序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private void JsonDeSerializer(string json)
        {
            var bytes = Encoding.UTF8.GetBytes(json);
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                var settings = new DataContractJsonSerializerSettings();
                settings.UseSimpleDictionaryFormat = true;
                var result = (new DataContractJsonSerializer(typeof(Dictionary<String, String>), settings).ReadObject(ms)) as Dictionary<String, String>;
                MessageData = result;
            }
        }

        /// <summary>
        /// 消息唯一标示
        /// </summary>
        public string Key
        {
            get
            {
                return MessageData?.Where(item => item.Key?.ToLower() == "key").FirstOrDefault().Value;
            }
        }

        /// <summary>
        /// 消息对应的用户UID
        /// </summary>
        public string UID
        {
            get
            {
                return MessageData?.Where(item => item.Key?.ToLower() == "uid").FirstOrDefault().Value;
            }
        }

        /// <summary>
        /// 有效时间（秒）
        /// </summary>
        public int ExpiresSeconds
        {
            get
            {
                var expires = MessageData.Where(item => item.Key?.ToLower() == "expires").FirstOrDefault().Value;
                if (string.IsNullOrWhiteSpace(expires))
                    return -1;

                return int.Parse(expires);
            }
        }

        /// <summary>
        /// 消息创建时间
        /// </summary>
        public String CreateDateTime
        {
            get
            {
                var time = MessageData.Where(item => item.Key?.ToLower() == "sendtime").FirstOrDefault().Value;
                //DateTimeFormat df = new DateTimeFormat("yyyy-MM-dd HH:mm:ss");
                //DateTime.Parse(time, df.FormatProvider);

                return time;
            }
        }
        
        /// <summary>
        /// 消息类型
        /// </summary>
        public String InfoType
        {
            get
            {
                var infotype = MessageData.Where(item => item.Key?.ToLower() == "infotype").FirstOrDefault().Value;
                return infotype;
            }
        }

        /// <summary>
        /// 是否是业务消息
        /// </summary>
        public bool IsBusinessMsg
        {
            get
            {
                return !string.IsNullOrWhiteSpace(UID) || InfoType?.ToLower() == "business";
            }
        }

        /// <summary>
        /// 推送类型（toast、tile、uwp），强制小写
        /// </summary>
        public String PushType
        {
            get
            {
                var type = MessageData.Where(item => item.Key?.ToLower() == "type").FirstOrDefault().Value?.ToLower();
                return type;
            }
        }


        /// <summary>
        /// 推送内容
        /// </summary>
        public string PushContent
        {
            get
            {
                return MessageData.Where(item => item.Key?.ToLower() == "content").FirstOrDefault().Value;
            }
        }


        /// <summary>
        /// 消息类型名称（如：订单通知、礼品兑换 等）
        /// </summary>
        public String InfoTypeName
        {
            get
            {
                return MessageData.Where(item => item.Key?.ToLower() == "infotypename").FirstOrDefault().Value?.ToLower();
            }
        }

        /// <summary>
        /// 消息是否已经过期
        /// </summary>
        /// <returns></returns>
        public bool IsTimeOut
        {
            get
            {
                var expiresSeconds = ExpiresSeconds;
                if (expiresSeconds < 0)
                    return false;
                
                var time = CreateDateTime;

                if (string.IsNullOrWhiteSpace(time))
                    return true;
                var createDateTime = DateTime.ParseExact(time, "yyyyMMddHHmmss", System.Globalization.CultureInfo.CurrentCulture);

                return (createDateTime.AddSeconds(expiresSeconds) <= DateTime.Now);
            }
        }

        /// <summary>
        /// 是否是当前用户的消息
        /// </summary>
        public bool IsCurrentUserMessage
        {
            get
            {
                var messageUID = UID?.Trim().ToLower();
                if (string.IsNullOrWhiteSpace(messageUID))
                    return true;

                var currentUID = "";//ToDO: new Core.Services.MessagesService().GetCurrentUserID();
                return (messageUID == currentUID);
            }
        }

        /// <summary>
        /// 消息数据内容
        /// </summary>
        Dictionary<String, String> MessageData;
    }

    /// <summary>
    /// 推送的消息UWP类型Content格式
    /// </summary>
    public sealed class PushedMessageInfoUwpContent
    {
        /// <summary>
        /// 当前消息内容的上级容器信息
        /// </summary>
        public PushedMessageInfo Parent { get; private set; }


        public PushedMessageInfoUwpContent(PushedMessageInfo pushedMessageInfo)
        {
            this.Parent = pushedMessageInfo;

            JsonDeSerializer(Parent.PushContent);
        }



        /// <summary>
        /// Json反序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private void JsonDeSerializer(string json)
        {
            var bytes = Encoding.UTF8.GetBytes(json);
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                var settings = new DataContractJsonSerializerSettings();
                settings.UseSimpleDictionaryFormat = true;
                var result = (new DataContractJsonSerializer(typeof(Dictionary<String, String>), settings).ReadObject(ms)) as Dictionary<String, String>;
                UwpMessageContent = result;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public String ContentType
        {
            get
            {
                var type = UwpMessageContent.ContainsKey("contenttype") ? UwpMessageContent["contenttype"] : null;
                return type;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String ContentTypeName
        {
            get
            {
                var type = UwpMessageContent.ContainsKey("contenttypename") ? UwpMessageContent["contenttypename"] : null;
                return type;
            }
        }

        public String MessageBody
        {
            get
            {
                var uwp = UwpMessageContent.ContainsKey("uwp") ? UwpMessageContent["uwp"] : null;
                return uwp;
            }   
        }

        public String AdditionalTileMsg
        {
            get
            {
                var tile = UwpMessageContent.ContainsKey("tile") ? UwpMessageContent["tile"] : null;
                return tile;
            }
        }
        public String AdditionalToastMsg
        {
            get
            {
                var toast = UwpMessageContent.ContainsKey("toast") ? UwpMessageContent["toast"] : null;
                return toast;
            }
        }
        public String AdditionalBadgeMsg
        {
            get
            {
                var badge = UwpMessageContent.ContainsKey("badge") ? UwpMessageContent["badge"] : null;
                return badge;
            }
        }

        Dictionary<String, String> UwpMessageContent;
    }
}
