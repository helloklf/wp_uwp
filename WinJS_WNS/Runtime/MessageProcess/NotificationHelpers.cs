﻿using System;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace FZS.Runtime.MessageProcess
{
    /// <summary>
    /// 磁贴通知帮助类
    /// </summary>
    public sealed class NotificationHelpers
    {
        const String MessageCenterUrl = "popup://RightSidebarsContainerPage/MessagesCenterPage";
        
        /// <summary>
        /// 清除磁贴
        /// </summary>
        public void ClearTile()
        {
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.Clear();
        }

        /// <summary>
        /// 设置磁贴内容
        /// </summary>
        /// <param name="tileXmlContent"></param>
        public void SetTileContent(String tileXmlContent)
        {
            var xmlDoc = new XmlDocument();
            TileNotification tn;
            try
            {
                xmlDoc.LoadXml(tileXmlContent);
                tn = new TileNotification(xmlDoc);
            }
            catch
            {
                throw new FormatException("解析XML失败！");
            }

            #region 显示磁贴
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.Clear();

            /*ToDo：判断是否开启动态磁贴
            if (Core.Services.ServiceAbstract.GetService<Core.Services.MessagesService>().GetDynamicTileOpenState() == false)
                return;
            */

            updater.EnableNotificationQueue(true);
            updater.EnableNotificationQueueForSquare150x150(true);
            updater.EnableNotificationQueueForSquare310x310(true);
            updater.EnableNotificationQueueForWide310x150(true);

            updater.Update(tn);
            #endregion
        }

        /// <summary>
        /// 设置磁贴内容（有失效时长）
        /// </summary>
        /// <param name="tileXmlContent"></param>
        /// <param name="expirationTimeSeconds">消息显示时长【秒】（小于1表示永久）</param>
        public void SetTileContent(String tileXmlContent, double expirationTimeSeconds)
        {
            var xmlDoc = new XmlDocument();
            TileNotification tn;
            try
            {
                xmlDoc.LoadXml(tileXmlContent);
                tn = new TileNotification(xmlDoc);
            }
            catch
            {
                throw new FormatException("解析XML失败！");
            }

            #region 显示磁贴
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.Clear();


            /*ToDo：判断是否开启动态磁贴
            if (Core.Services.ServiceAbstract.GetService<Core.Services.MessagesService>().GetDynamicTileOpenState() == false)
                return;
            */

            //设置过期时间
            if (expirationTimeSeconds > 0)
                tn.ExpirationTime = new DateTimeOffset(DateTime.Now.AddSeconds(expirationTimeSeconds));

            updater.EnableNotificationQueue(true);
            updater.EnableNotificationQueueForSquare150x150(true);
            updater.EnableNotificationQueueForSquare310x310(true);
            updater.EnableNotificationQueueForWide310x150(true);

            updater.Update(tn);
            #endregion
        }

        /// <summary>
        /// 设置Toast通知
        /// </summary>
        /// <param name="toastXmlContent"></param>
        public void SetToastContent(String toastXmlContent)
        {
            var xmlDoc = new XmlDocument();
            ToastNotification tn;
            try
            {

                xmlDoc.LoadXml(toastXmlContent);
                tn = new ToastNotification(xmlDoc);
            }
            catch
            {
                throw new FormatException("解析XML失败！");
            }

            ToastNotificationManager.CreateToastNotifier().Show(tn);
        }

        /// <summary>
        /// 新通知提醒
        /// </summary>
        public void NewMessageNotify()
        {
            SetToastContent("<toast launch='" + MessageCenterUrl + "'><visual><binding template='ToastGeneric'  arguments='yes'><text>您收到一条新消息，点此查看消息内容！</text><image placement='appLogoOverride' src='ms-appx:///assets/storeLogo.png' /></binding></visual></toast>");
        }

        /// <summary>
        /// 设置Badge通知内容
        /// </summary>
        /// <param name="badgeXmlContent"></param>
        public void SetBadgeContent(String badgeXmlContent)
        {
            var xmlDoc = new XmlDocument();
            BadgeNotification bn;
            try
            {
                xmlDoc.LoadXml(badgeXmlContent);
                bn = new BadgeNotification(xmlDoc);
            }
            catch
            {
                throw new FormatException("解析XML失败！");
            }
            var badge = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            badge.Clear();
            badge.Update(bn);
        }
    }
}
