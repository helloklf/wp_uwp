﻿using FZS.Runtime.MessageProcess.Status;
using System;
using Windows.Foundation.Metadata;

namespace FZS.Runtime.MessageProcess
{
    /// <summary>
    /// 消息处理任务
    /// </summary>
    [AllowForWeb]
    public sealed partial class RawMessageProcess
    {
        /// <summary>
        /// 事件
        /// </summary>
        public IRawMessageProcessEvents Events { get; set; }

        /// <summary>
        /// 消息处理-原始通知
        /// </summary>
        public void OpenMessage(String rawMsgContent)
        {
            var msgID = "Unknown";
            var isBusinessMsg = false;
            try
            {
                //消息内容解析
                var msg = new PushedMessageInfo(rawMsgContent);

                //尝试获取消息ID
                msgID = msg?.Key ?? msgID;
                //尝试获取是否是业务消息
                isBusinessMsg = msg != null ? msg.IsBusinessMsg : isBusinessMsg;

                #region 用户校验
                if (!msg.IsCurrentUserMessage)
                {
                    Feedback(MessageStatusEnum.mistake, "该消息不是当前用户的！", msgID, true);
                    return;
                }
                #endregion

                #region 过期校验
                if (msg.IsTimeOut)
                {
                    Feedback(MessageStatusEnum.timeout, "该消息已经过期，客户端已自动将其过滤，不再显示！", msgID, false);
                    return;
                }
                #endregion

                var msgType = PushTypeEnum.Unknown;
                Enum.TryParse(msg.PushType, true, out msgType);
                
                var msgContent = msg.PushContent;
                #region 消息分类处理
                switch (msgType)
                {
                    case PushTypeEnum.Toast:
                        {
                            RawMessageContentToast(msg);
                            break;
                        }
                    case PushTypeEnum.Tile:
                        {
                            RawMessageContentTile(msg);
                            break;
                        }
                    case PushTypeEnum.Badge:
                        {
                            RawMessageContentBadge(msg);
                            break;
                        }
                    case PushTypeEnum.UWP:
                        {
                            UWPMessageProcess(msg);
                            break;
                        }
                    default:
                        {
                            Events?.OnUnknownMessageReceived(msg);
                            throw new FormatException("消息格式错误，type参数不在指定范围内，应为【toast、tile、badge、uwp】其中之一（不区分大小写），当前客户端消息协议版本：v1.0");
                        }
                }
                #endregion

                Feedback(MessageStatusEnum.received, "消息已接收", msg.Key, (!String.IsNullOrWhiteSpace(msg.UID)));
            }
            //消息格式错误
            catch (FormatException ex)
            {
                Feedback(MessageStatusEnum.formaterror, ex.Message, msgID, isBusinessMsg);
            }
            //消息处理异常
            catch (Exception ex)
            {
                Feedback(MessageStatusEnum.formaterror, "消息处理异常，可能是格式错误。\r\n" + ex.Message, msgID, isBusinessMsg);
            }
        }


        #region 内部方法-消息分类处理和存储
        /// <summary>
        /// 消息处理-Toast通知
        /// </summary>
        /// <param name='pushedMsg'></param>
        void RawMessageContentToast(PushedMessageInfo pushedMsg)
        {
            new NotificationHelpers().SetToastContent(pushedMsg.PushContent);

            //存储消息
            MessageStorage(pushedMsg);

            //触发消息接收事件
            Events?.OnToastMessageReceived(pushedMsg);
        }

        /// <summary>
        /// 消息处理-Tile通知
        /// 调用此方法前，请确保消息未过期，否则会导致消息永久有效，而不会立即隐藏
        /// </summary>
        /// <param name='pushedMsg'></param>
        void RawMessageContentTile(PushedMessageInfo pushedMsg)
        {
            if (pushedMsg.ExpiresSeconds >= 0)
            {
                var time = DateTime.ParseExact(pushedMsg.CreateDateTime, "yyyyMMddHHmmss", System.Globalization.CultureInfo.CurrentCulture);
                var seconds = (time.AddSeconds(pushedMsg.ExpiresSeconds) - DateTime.Now).TotalSeconds;
                new NotificationHelpers().SetTileContent(pushedMsg.PushContent, seconds);
            }
            else
            {
                new NotificationHelpers().SetTileContent(pushedMsg.PushContent);
            }

            //触发消息接收事件
            Events?.OnTileMessageReceived(pushedMsg);
        }

        /// <summary>
        /// 消息处理-Badge通知
        /// </summary>
        /// <param name='pushedMsg'></param>
        void RawMessageContentBadge(PushedMessageInfo pushedMsg)
        {
            new NotificationHelpers().SetBadgeContent(pushedMsg.PushContent);

            //触发消息接收事件
            Events?.OnBadgeMessageReceived(pushedMsg);
        }

        /// <summary>
        /// 消息处理-UWP通知
        /// </summary>
        /// <param name='pushedMsg'></param>
        void UWPMessageProcess(PushedMessageInfo pushedMsg)
        {
            MessageStorage(pushedMsg);
            new NotificationHelpers().NewMessageNotify();

            Events?.OnUwpMessageReceived(pushedMsg);
        }

        /// <summary>
        /// 消息存储（仅限于UWP通知、Toast通知）
        /// </summary>
        /// <param name='msg'></param>
        void MessageStorage(PushedMessageInfo msg)
        {
            var time = DateTime.ParseExact(msg.CreateDateTime, "yyyyMMddHHmmss", System.Globalization.CultureInfo.CurrentCulture);

            /*ToDo：消息存储
            new Core.Services.MessagesService().DB_InsertMessage(
               msg.Key,
               msg.PushContent,
               msg.InfoType,
               msg.InfoTypeName ?? "消息通知",
               msg.IsBusinessMsg ? NotifyMsgTypeEnum.Business : NotifyMsgTypeEnum.Platform,
               time,
               msg.ExpiresSeconds,
               msg.UID,
               Contracts.Models.Wns.MessageStatusEnum.Received
            );*/
        }

        #endregion


        /// <summary>
        /// 反馈消息状态
        /// </summary>
        /// <param name="status">状态码</param>
        /// <param name="statusDesc">状态说明</param>
        /// <param name="msgID">消息ID</param>
        /// <param name="isBusinessMsg">是否是业务消息</param>
        public async void Feedback(MessageStatusEnum status, String statusDesc, String msgID, Boolean isBusinessMsg)
        {
            var deviceInfo = Guid.NewGuid().ToString();//ToDo: await Common.Helpers.SystemHelper.GetDeviceInfo();
            /*
            "infoid":"NKKAJSDL-21LKLAS-2KLJLJ-JIKUJI9",
            "deviceid":"UDNEOOF9387UDIHSK",
            "feedback":"opened",
            "infomsg":"用户已读"
            */

            //获取状态码（文本）
            var statusCode = Enum.GetName(status.GetType(), status).ToLower();

            //如果未提供状态说明，自动获取状态说明
            if (String.IsNullOrWhiteSpace(statusDesc))
                statusDesc = MessageStatusCodes.MessageStatusEnumToDesc(status);

            #region 执行数据操作
            //ToDo:提交反馈
            //await new Core.Services.MessagesService().PostMessageFeedback(msgID, statusCode, statusDesc, isBusinessMsg);
            #endregion
        }
    }
}
