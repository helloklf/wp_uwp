﻿using System;
using System.Linq;
using Windows.ApplicationModel.Background;

namespace FZS.Runtime.MessageProcess
{
    /// <summary>
    /// 注册后台任务，以便于在应用程序关闭后仍可以继续接收和处理通知
    /// </summary>
    public sealed class BackgroundTaskHelper
    {      
        /// <summary>
        /// 请求后台权限并注册后台任务
        /// </summary>
        public async void RegisterBackgroundTasks()
        {
            // 判断一下是否允许访问后台任务
            var status = await BackgroundExecutionManager.RequestAccessAsync();
            if (status == BackgroundAccessStatus.Denied || status == BackgroundAccessStatus.Unspecified)
            {
                //被拒绝后台运行
            }
            else
            {
                RegisterToastBackgroundTask();
                //RegisterToastActionBackgroundTask();    //目前无此需求
                //RegisterScheduledTask();                          //目前无此需求
            }
        }

        /// <summary>
        /// 注册Raw通知后台接收处理任务
        /// </summary>
        void RegisterToastBackgroundTask()
        {
            // 声明触发器
            var trigger = new PushNotificationTrigger();
            UninstallRegisterTask<Runtime.ToastBackgroundTask>(trigger);
        }

        /// <summary>
        /// 注册Toast通知后台动作处理任务
        /// </summary>
        void RegisterToastActionBackgroundTask()
        {
            // 声明触发器
            var trigger = new ToastNotificationActionTrigger();
            UninstallRegisterTask<Runtime.ToastActionBackgroundTask>(trigger);
        }

        /// <summary>
        /// 注册后台定时任务
        /// </summary>
        void RegisterScheduledTask()
        {
            uint interval = 15;//时间间隔（分钟），最小15分钟
            var trigger = new TimeTrigger(interval, false);
            UninstallRegisterTask<Runtime.ScheduledTask>(trigger);
        }

        /// <summary>
        /// 卸载并重新注册后台任务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="trigger"></param>
        /// <returns></returns>
        bool UninstallRegisterTask<T>(IBackgroundTrigger trigger)  where T: IBackgroundTask
        {
            var taskType = typeof(T);
            var registedTask = BackgroundTaskRegistration.AllTasks.Values.FirstOrDefault(t => t.Name == taskType.FullName);

            // 声明触发器
            try
            {
                //卸载已注册的任务
                if (registedTask != null)
                    registedTask.Unregister(true);

                //注册后台服务
                var tBuilder = new BackgroundTaskBuilder();
                tBuilder.Name = taskType.FullName;
                tBuilder.TaskEntryPoint = taskType.FullName;
                tBuilder.SetTrigger(trigger);
                tBuilder.Register();
                return true;
            }
            catch
            {
                return false;
            }
        }
      
    }
}
