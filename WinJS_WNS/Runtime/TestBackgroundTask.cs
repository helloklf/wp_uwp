﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Foundation.Metadata;

namespace FZS.Runtime
{
    [AllowForWeb]
    public sealed class TestBackgroundTask: IBackgroundTrigger
    {
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            var deferral = taskInstance.GetDeferral();

            var doc = new XmlDocument();
            doc.LoadXml("<toast launch=\"app-defined-string\"><visual>        <binding template=\"ToastGeneric\">          <text>Sample</text>          <text>This is a simple toast notification example</text>          <image placement=\"AppLogoOverride\" src=\"oneAlarm.png\" />        </binding>      </visual>      <actions>        <action content=\"check\" arguments=\"check\" imageUri=\"check.png\" />        <action content=\"cancel\" arguments=\"cancel\" />      </actions>      <audio src=\"ms-winsoundevent:Notification.Reminder\"/>    </toast>");
            var tn = new Windows.UI.Notifications.ToastNotification(doc);

            var tnm = Windows.UI.Notifications.ToastNotificationManager.CreateToastNotifier();
            tnm.Show(tn);

            deferral.Complete();
        }

        public void Register()
        {
            var name = this.GetType().FullName; ;
            var tb = new BackgroundTaskBuilder();
            var tasks = BackgroundTaskRegistration.AllTasks.Values.Where(item => item.Name == name).FirstOrDefault();
            if(tasks==null)
                tasks.Unregister(true);

            tb.SetTrigger(new PushNotificationTrigger());
            tb.TaskEntryPoint = name;
            tb.Name = name;
            tb.Register();
        }
    }
}
