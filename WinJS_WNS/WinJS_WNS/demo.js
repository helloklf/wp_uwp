﻿var trigger = new Windows.ApplicationModel.Background.PushNotificationTrigger();
//BackgroundTaskHelper.RegisterBackgroundTask(trigger);

//调用例子-Toast
var toast = NotificationHelper.SetToastNotification(
    '<toast launch="app-defined-string">\
      <visual>\
        <binding template="ToastGeneric">\
          <text>Sample</text>\
          <text>This is a simple toast notification example</text>\
          <image placement="AppLogoOverride" src="oneAlarm.png" />\
        </binding>\
      </visual>\
      <actions>\
        <action content="check" arguments="check" imageUri="check.png" />\
        <action content="cancel" arguments="cancel" />\
      </actions>\
      <audio src="ms-winsoundevent:Notification.Reminder"/>\
    </toast>',
    function (arg) {
        console.log(arg);
    }
);

//setTimeout(function () { NotificationHelper.HideToast(toast); }, 3000);//3秒后隐藏通知

//调用例子-Tile通知
NotificationHelper.SetTileNotification(
    "\<tile>\
        <visual>\
            <binding template='TileMedium'>\
                <text hint-style='subtitle'>科技新闻</text>\
                <text hint-style='captionSubtle'>华为Mate9 今日发布</text>\
                <text hint-style='captionSubtle'>华为Mate9 售价或超1999美元</text>\
            </binding>\
            <binding template='TileWide'>\
                <text hint-style='subtitle'>科技新闻</text>\
                <text hint-style='captionSubtle'>华为Mate9 今日发布</text>\
                <text hint-style='captionSubtle'>华为Mate9 售价或超1999美元</text>\
            </binding>\
        </visual>\
    </tile>"
);

//调用例子-Badge通知
//NotificationHelper.SetBadgeNotification("<badge value=\"100\" />");


//开始接收消息
NotificationHelper.BeginReceive(
    function OnChannelGeted(channel) {
        var uriString = channel.uri.toString();
        $("#URI").val(uriString);
        var xr = new XMLHttpRequest();
        xr.onreadystatechange = function () {
            if (xr.status === 200 && xr.readyState === 4) {
                console.log(xr.response);
            }
        };
        xr.open("POST", "http://localhost/test.ashx", true);
        xr.send(new FormData({
            channelURI: uriString,
            type: "toast",
            content: "aweq288q3812938129312839"
        }));

        //server.api.wns.uploadChannel(  uriString ); //完成此代码段，将url上传到云端服务器
        //localSettings.values["localWNSChannel"] = uriString; //存储消息通道，以便于检查通道是否变更
    },
    function OnMessageRecived(msg) {
        var notifyType = msg.notificationType;
        switch (notifyType) {
            case Windows.Networking.PushNotifications.PushNotificationType.raw: {
                //接收到Raw通知
                var rawMsg = msg.rawNotification;
                //消息处理
                new FZS.Runtime.MessageProcess.RawMessageProcess().openMessage(rawMsg.content);
                $("#PUSHCONTENT").val(rawMsg.content);
                console.log(msg);
                break;
            }
            case Windows.Networking.PushNotifications.PushNotificationType.badge: {
                //接收到Badge通知
                var badgeMsg = msg.badgeNotification;
                var bnm = Windows.UI.Notifications.BadgeUpdateManager.createBadgeUpdaterForApplication();
                bnm.update(badgeMsg);
                break;
            }
            case Windows.Networking.PushNotifications.PushNotificationType.tile: {
                //接收到Tile通知
                var tileMsg = msg.tileNotification;
                var tnm = Windows.UI.Notifications.TileUpdateManager.createTileUpdaterForApplication();
                tnm.update(tileMsg);
                break;
            }
            case Windows.Networking.PushNotifications.PushNotificationType.toast: {
                //接收到Toast通知
                var toastMsg = msg.toastNotification;
                var tnm2 = Windows.UI.Notifications.ToastNotificationManager.createToastNotifier();
                tnm2.update(toastMsg);
                break;
            }
            default: {
                //未知类型-除非有新增的推送方式-否则将不会有此类型的结果
                break;
            }
        }
    }
);




new Runtime.TestBackgroundTask().register();
var titleBar = Windows.UI.ViewManagement.ApplicationView.getForCurrentView().titleBar;
titleBar.inactiveBackgroundColor = titleBar.buttonBackgroundColor = titleBar.buttonInactiveBackgroundColor = titleBar.backgroundColor = { a: 255, b: 0, g: 196, r: 245 };
titleBar.buttonHoverBackgroundColor = { a: 255, b: 70, g: 210, r: 244 };
titleBar.buttonPressedBackgroundColor = { a: 255, b: 0, g: 160, r: 224 };
titleBar.foregroundColor = Windows.UI.Colors.black;
titleBar.inactiveForegroundColor = titleBar.buttonInactiveForegroundColor = titleBar.buttonPressedForegroundColor = titleBar.buttonHoverForegroundColor = titleBar.buttonForegroundColor = { a: 255, b: 0, g: 123, r: 181 };

//显示返回按钮
Windows.UI.Core.SystemNavigationManager.getForCurrentView().appViewBackButtonVisibility = Windows.UI.Core.AppViewBackButtonVisibility.visible;
//页面标题
Windows.UI.ViewManagement.ApplicationView.getForCurrentView().title = document.title;
//Windows.UI.Core.AppViewBackButtonVisibility

var postChannelUriApi = "http://localhost/index.html";
var map = new Windows.Foundation.Collections.StringMap();
map.insert("uid", "13178638515");
map.insert("pwd", "asdf1234");

var http = new Windows.Web.Http.HttpClient();
var form = new Windows.Web.Http.HttpFormUrlEncodedContent(map);
var result = http.postAsync(new Windows.Foundation.Uri(postChannelUriApi), form);
result
    .then(
        function (s) {
            //完成
            console.log("提交通道完成");
        },
        function (e) {
            console.log("提交通道失败");
            //异常
        }
    )
    .done(
        function (s) { },
        function (e) { }
    );