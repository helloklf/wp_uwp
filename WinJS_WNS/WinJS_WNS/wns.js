﻿/// <var>提供一些通知的帮助方法</var> 
var NotificationHelper = {
    SetToastNotification: function SetToastNotification(xml, onClick) {
        /// <summary>设置Toast（Windows10右侧操作中心的）通知</summary>
        /// <param name="xml" type="String">toast通知xml内容</param>
        /// <param name="onClick" type="Function">toast通知点击回传</param>
        /// <returns type="Windows.UI.Notifications.ToastNotification">Toast通知</returns>

        var doc = new Windows.Data.Xml.Dom.XmlDocument();
        doc.loadXml(xml);

        var tn = new Windows.UI.Notifications.ToastNotification(doc);
        tn.onactivated = function (e) {
            var args = e.arguments;
            onClick ? onClick(args) : null;
        };
        var tnm = Windows.UI.Notifications.ToastNotificationManager.createToastNotifier();
        tnm.show(tn);

        return tn;
    },

    SetTileNotification: function (xml, expirationTime) {
        /// <signature>
        ///     <summary>设置Tile（Windows10开始菜单磁贴）通知</summary>
        ///     <param name="xml" type="String">tile通知xml内容</param>
        ///     <returns type="void">无返回值</returns>
        /// </signature>
        /// <signature>
        ///     <summary>设置Tile（Windows10开始菜单磁贴）通知</summary>
        ///     <param name="xml" type="String">tile通知xml内容</param>
        ///     <param name="expirationTime" type="Date">有效时间</param>
        ///     <returns type="Windows.UI.Notifications.TileNotification">Tile通知</returns>
        /// </signature>
        

        var doc = new Windows.Data.Xml.Dom.XmlDocument();
        doc.loadXml(xml);

        var tn = new Windows.UI.Notifications.TileNotification(doc);
        if (expirationTime)
            tn.expirationTime = expirationTime;
        var tnm = Windows.UI.Notifications.TileUpdateManager.createTileUpdaterForApplication();
        tnm.enableNotificationQueueForSquare150x150 = true;
        tnm.enableNotificationQueueForSquare310x310 = true;
        tnm.enableNotificationQueueForWide310x150 = true;
        tnm.clear();
        tnm.enableNotificationQueue(true);
        tnm.update(tn);

        return tn;
    },

    SetBadgeNotification: function (xml, expirationTime) {
        /// <summary>设置Badge（徽章）通知</summary>
        /// <param name="xml" type="String">Badge通知xml内容</param>
        /// <param name="expirationTime" type="Date">有效时间</param>
        /// <returns type="Windows.UI.Notifications.BadgeNotification">Badge通知</returns>

        var doc = new Windows.Data.Xml.Dom.XmlDocument();
        doc.loadXml(xml);

        var bn = new Windows.UI.Notifications.BadgeNotification(doc);
        if (expirationTime)
            bn.expirationTime = expirationTime;
        var bnm = Windows.UI.Notifications.BadgeUpdateManager.createBadgeUpdaterForApplication();
        bnm.update(bn);

        return bn;
    },

    ClearTile: function ClearTiles() {
        /// <summary>清除动态磁贴内容</summary>
        /// <returns type="void">无返回值</returns>

        var tnm = Windows.UI.Notifications.TileUpdateManager.createTileUpdaterForApplication();
        tnm.clear();
    },

    HideToast: function HideToast(toast) {
        /// <summary>清除Toast通知</summary>
        /// <param name="toast" type="Windows.UI.Notifications.ToastNotification">Toast通知</param>
        /// <returns type="void">无返回值</returns>

        var tnm = Windows.UI.Notifications.ToastNotificationManager.createToastNotifier();
        tnm.hide(toast);
    },

    BeginReceive: function (onChannelGeted, onMessageReceived) {
        /// <summary>清除Toast通知</summary>
        /// <param name="onChannelGeted" type="Function">当消息推送通道获取成功</param>
        /// <param name="onMessageReceived" type="Function">当消息接收时</param>
        /// <returns type="void">无返回值-异步方法所有结果均通过回调获取</returns>
        Windows.Networking.PushNotifications.PushNotificationChannelManager.createPushNotificationChannelForApplicationAsync().done(
            function (channel) {
                if (onChannelGeted)
                    onChannelGeted(channel);
                //x channel.close();//关闭消息通道，下次获取消息通道将变更新的通道
                console.log(channel.uri.toString());
                channel.onpushnotificationreceived = onMessageReceived;
            }
        );
    }
};

/// <var>提供一些方法便于快速注册和卸载后台任务</var>
var BackgroundTaskHelper = {
    UnRegisterBackgroundTask: function (registedTaskGuid) {
        /// <summary>设置Toast（Windows10右侧操作中心的）通知</summary>
        /// <param name="registedTaskGuid" type="String">已注册的taskID</param>
        /// <returns type="Boolen">是否卸载成功</returns>

        try{
            if (!registedTaskGuid)
                return false;

            var tasks = Windows.ApplicationModel.Background.BackgroundTaskRegistration.allTasks;
            var task;
        
            var item = tasks.lookup(registedTaskGuid);
            if (item != null) {
                item.unregister(true);
                return true;
            }
        }
        catch (e) {  }

        return false;
    },
    RegisterBackgroundTask: function (trigger, entryPoint) {
        /// <summary>设置Toast（Windows10右侧操作中心的）通知</summary>
        /// <param name="trigger" type="Windows.ApplicationModel.Background.IBackgroundTrigger">触发器类型</param>
        /// <param name="entryPoint" type="String">入口点</param>
        /// <returns type="void"></returns>
        var Background = Windows.ApplicationModel.Background;

        var promise = Background.BackgroundExecutionManager.requestAccessAsync().then(
            function (result) {
                switch (result) {
                    case Background.BackgroundAccessStatus.denied:
                        // Windows: Background activity and updates for this app are disabled by the user.
                        //
                        // Windows Phone: The maximum number of background apps allowed across the system has been reached or
                        // background activity and updates for this app are disabled by the user.
                        break;

                    case Background.BackgroundAccessStatus.allowedWithAlwaysOnRealTimeConnectivity:

                        // Windows: Added to list of background apps; set up background tasks; 
                        // can use the network connectivity broker.
                        //
                        // Windows Phone: This value is never used on Windows Phone.
                        break;

                    case Background.BackgroundAccessStatus.allowedMayUseActiveRealTimeConnectivity:
                        // Windows: Added to list of background apps; set up background tasks;
                        // cannot use the network connectivity broker.
                        //
                        // Windows Phone: This app can register background tasks. Required for all 
                        // background tasks on Windows Phone.
                        break;

                    case Background.BackgroundAccessStatus.unspecified:
                        // The user didn't explicitly disable or enable access and updates. 
                        break;
                }


                var taskID = localStorage.getItem("taskID");
                BackgroundTaskHelper.UnRegisterBackgroundTask(taskID);
                try {
                    var taskBuilder = Windows.ApplicationModel.Background.BackgroundTaskBuilder();
                    taskBuilder.name = "Runtime.TestBackgroundTask";
                    taskBuilder.taskEntryPoint = "Runtime.TestBackgroundTask";
                    taskBuilder.setTrigger(trigger);
                    var registedTask = taskBuilder.register();
                    var taskID = registedTask.taskId;
                    console.log(taskID);
                    localStorage.setItem("taskID", taskID);
                }
                catch (e) {
                    console.log(e.message);
                }
            });
    }
};