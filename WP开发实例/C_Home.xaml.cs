﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// “用户控件”项模板在 http://go.microsoft.com/fwlink/?LinkId=234236 上提供

namespace 易职网
{
    public sealed partial class C_Home : UserControl
    {
        public C_Home()
        {
            this.InitializeComponent();
        }

        private void FlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try 
            {
                if (Xaml_Flip!=null)
                    Xaml_FlipState.Text = (Xaml_Flip.SelectedIndex+1) + " / " + Xaml_Flip.Items.Count;
            }
            catch
            {

            }
        }

        private void Xaml_Flip_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Xaml_Flip != null)
                    Xaml_FlipState.Text = (Xaml_Flip.SelectedIndex + 1) + " / " + Xaml_Flip.Items.Count;
            }
            catch
            {

            }
        }

        /// <summary>
        /// 转到登陆页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoLoginPage_Click(object sender, TappedRoutedEventArgs e)
        {
            if (NavigateHelper.NavigateToLogin!=null)
                NavigateHelper.NavigateToLogin();
        }

        /// <summary>
        /// 转到注册页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoRegisterPage_Click(object sender, TappedRoutedEventArgs e)
        {
            if (NavigateHelper.NavigateToRegister != null)
                NavigateHelper.NavigateToRegister();
        }
    }
}
