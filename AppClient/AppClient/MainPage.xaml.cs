﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.AppService;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

//“空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409 上有介绍

namespace AppClient
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void xx_Click(object sender, RoutedEventArgs e)
        {
            using (var connection = new AppServiceConnection())
            {
                //Set up a new app service connection

                //connection.AppServiceName = "com.microsoft.randomnumbergenerator";
                //connection.PackageFamilyName = "Microsoft.SDKSamples.AppServicesProvider.CS_8wekyb3d8bbwe";

                connection.AppServiceName = "com.devtest.com";
                connection.PackageFamilyName = "62d8531e-3e36-47ce-ba6f-a5ba12c7a80a_95s22j1hnkj2w";// "3c0f878a-1fe8-48f4-a16d-e12ad14d8314_95s22j1hnkj2w";
                AppServiceConnectionStatus status = await connection.OpenAsync();

                //The new connection opened successfully
                if (status == AppServiceConnectionStatus.Success)
                {

                }

                //If something went wrong. Lets figure out what it was and show the 
                //user a meaningful message and walk away
                switch (status)
                {
                    case AppServiceConnectionStatus.AppNotInstalled:
                        return;

                    case AppServiceConnectionStatus.AppUnavailable:
                        return;

                    case AppServiceConnectionStatus.AppServiceUnavailable:
                        return;

                    case AppServiceConnectionStatus.Unknown:
                        return;
                }

                //Set up the inputs and send a message to the service
                var inputs = new ValueSet();
                inputs.Add("minvalue", 12);
                inputs.Add("maxvalue", 55);
                AppServiceResponse response = await connection.SendMessageAsync(inputs);

                //If the service responded with success display the result and walk away
                if (response.Status == AppServiceResponseStatus.Success &&
                    response.Message.ContainsKey("result"))
                {
                    var resultText = response.Message["result"].ToString();
                    if (!string.IsNullOrEmpty(resultText))
                    {
                        await new MessageDialog(resultText).ShowAsync();

                        connection.ServiceClosed += Connection_ServiceClosed;
                        //connection.Dispose();
                    }
                    else
                    {

                    }

                    return;
                }

                //Something went wrong while sending a message. Let display
                //a meaningful error message
                switch (response.Status)
                {
                    case AppServiceResponseStatus.Failure:
                        return;

                    case AppServiceResponseStatus.ResourceLimitsExceeded:
                        return;

                    case AppServiceResponseStatus.Unknown:
                        return;
                }
            }
        }

        private void Connection_ServiceClosed(AppServiceConnection sender, AppServiceClosedEventArgs args)
        {
            throw new NotImplementedException();
        }
    }
}
