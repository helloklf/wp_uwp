﻿(function () {

    // This var is used to get information about the current instance of the background task.
    var backgroundTaskInstance = Windows.UI.WebUI.WebUIBackgroundTaskInstance.current;
    //Windows.System.Launcher.launchUriAsync(new Windows.Foundation.Uri("https://baidu.com"));


    var deferral = backgroundTaskInstance.getDeferral();

    // Trigger details contain the information surrounding the reason why the this task was triggered
    var details = backgroundTaskInstance.triggerDetails;//AppServiceTriggerDetails
    backgroundTaskInstance.addEventListener("canceled", function (o) {
        WriteFile("TaskCanceled.txt", new Date().toLocaleTimeString());
    });


    //var conn = new Windows.ApplicationModel.AppService.AppServiceConnection();
    var connection = details.appServiceConnection;

    function OnAppServiceRequestReceived(obj) {
        //Windows.ApplicationModel.AppService.AppServiceRequestReceivedEventArgs
        var deferral = obj.getDeferral();

        for (var i = 0; i < obj.detail.length; i++) {
            var messages = obj.detail[i].request.message; //Windows.Foundation.Collections.ValueSet
            var messageInfo = {
                "minvalue": messages["minvalue"],
                "maxvalue": messages["maxvalue"]
            };

            var result = new Windows.Foundation.Collections.ValueSet();
            result.insert("result", messageInfo.minvalue + messageInfo.maxvalue);
            obj.detail[i].request.sendResponseAsync(result);
        }

        deferral.complete();
    }


    function AppServiceConnection_ServiceClosed(obj) {
        WriteFile("ConnectionClosed.txt", new Date().toLocaleTimeString());
    }

    function WriteFile(fileName,textContent) {
        var file = Windows.Storage.ApplicationData.current.localFolder.createFileAsync(fileName);
    }

    //connection.__proto__.onserviceclosed
    connection.addEventListener("serviceclosed", AppServiceConnection_ServiceClosed);
    connection.addEventListener("requestreceived", OnAppServiceRequestReceived);

    deferral.complete();
    connection.close();

})();
